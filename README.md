# README #

A PHP CLI program displaying the products of primes up to the nth in a table.

### Setup ###

* Getting the source code
~~~~
git clone git@bitbucket.org:forbdn/questers_challenge.git
~~~~

* Composer dependencies
~~~~
composer install
composer dump-autoload
~~~~

### Running the PHP CLI program ###

~~~
php primes.php --help
~~~
This will output the help screen for the CLI program

~~~
php primes.php <OPTIONS> <nth_prime_index> [<first_prime_index>]
~~~

For example if you want to find the first 10 prime numbers:
~~~
php primes.php 10
~~~
When the displayed table gets too big you have to partition the results
in order to display it properly. In order to scale well you have to provide 
2 prime index numbers to and from:
~~~
php primes.php 40 30
~~~

###Complexity and Performance
The algorithm used is the "Sieve of Atkin", a bit different and optimized version of "Erathosten's Sieve".
It's complexity is O(N / (log log N)).

You can test the performance of the program yourself by running it with --perf
~~~
php primes.php --perf 20
~~~
It will display a nice table with some time measurements. 
It uses https://github.com/bvanhoekelen/performance PHP library for measuring performance.
If we want to optimize the program's performance we have to take exact measurements and check them against any changes made.

### TDD & Tests
You can run the unit tests via:
~~~
vendor/phpunit/phpunit/phpunit
~~~

From the git commits history you can trace the projects TDD implementation. 
It started with "tests first" approach and added functionality and tests in parallel.