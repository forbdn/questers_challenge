<?php
namespace Questers;

/**
 * Class AtkinSieve
 * @package Questers
 *
 * In mathematics, the sieve of Atkin is a modern algorithm for finding all prime numbers
 * up to a specified integer. Compared with the ancient sieve of Eratosthenes, which marks
 * off multiples of primes, the sieve of Atkin does some preliminary work and then marks off
 * multiples of squares of primes, thus achieving a better theoretical asymptotic complexity.
 * It was created in 2003 by A. O. L. Atkin and Daniel J. Bernstein.[1]
 * https://en.wikipedia.org/wiki/Sieve_of_Atkin
 */
class AtkinSieve {
    const FIRST_FIVE_PRIMES = [2, 3, 5, 7, 11];
    const PRIMES_THEOREM_THRESHOLD = 6;

    /**
     * @param $nth_prime_index - The prime index number to end to. Required!
     * @param $first_prime_index - The first prime index number to start from. Optional, Default 0.
     * @return array - The filtered prime numbers
     */
    public function findPrimes($nth_prime_index, $first_prime_index = 0) {
        /*
         * If we search for primes up to the 6th prime - no need to use the sieve of Atkin.
         */
        if ($nth_prime_index < self::PRIMES_THEOREM_THRESHOLD) {
            return array_slice(self::FIRST_FIVE_PRIMES, $first_prime_index, $nth_prime_index - $first_prime_index);
        }

        /*
         * Sieve of Atkin gives you all the primes up to a specified integer.
         * For primes from the 6th and above - we need to calculate the upper bound
         * for the sieve of Atkin. It is based on a formula from the Primes theorem.
         * The number PRIMES_THEOREM_THRESHOLD(6) here is a threshold from the theorem.
         */
        $limit = (int) ($nth_prime_index * (log($nth_prime_index) + log (log($nth_prime_index))));
        //echo $limit;
        /*
         * The Sieve of Atkin implementation
         */
        $sieve = array_fill(0, $limit,false);
        $sieve[0] = $sieve[1] = false;
        $sieve[2] = $sieve[3] = true;
        $limitSqrt = (int) sqrt($limit);
        for($x = 1; $x <= $limitSqrt; $x++) {
            for($y = 1; $y <= $limitSqrt; $y++) {
                $n = (4 * $x * $x) + ($y * $y);
                if ($n <= $limit && ($n % 12 == 1 || $n % 12 == 5) && isset($sieve[$n])) {
                    $sieve[$n] = !$sieve[$n];
                }

                $n = (3 * $x * $x) + ($y * $y);
                if ($n <= $limit && ($n % 12 == 7) && isset($sieve[$n])) {
                    $sieve[$n] = !$sieve[$n];
                }
                $n = (3 * $x * $x) - ($y * $y);
                if ($x > $y && $n <= $limit && ($n % 12 == 11) && isset($sieve[$n])) {
                    $sieve[$n] = !$sieve[$n];
                }
            }
        }

        for ($i = 5; $i <= $limitSqrt; $i++) {
            if ($sieve[$i]) {
                $x = $i * $i;
                for ($j = $x; $j <= $limit; $j += $x) {
                    $sieve[$j] = false;
                }
            }
        }

        /*
         * Getting all the actual primes from the keys of the $sieve dictionary.
         * We have to filter them in order to get only the ones flagged true (or prime).
         */
        $primes = array_keys(
            array_filter($sieve, function($is_prime_flag) {
                return $is_prime_flag;
            })
        );

        /*
         * There's a mathematical mistake sometimes coming from the upper limit.
         * That's why we need to get only the needed subset.
         */
        return array_slice($primes, $first_prime_index, $nth_prime_index - $first_prime_index);
    }
}

