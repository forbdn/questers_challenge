<?php
require 'vendor/autoload.php';

use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Options;
use splitbrain\phpcli\TableFormatter;
use Performance\Performance;
use Questers\AtkinSieve;

class PrimesTableCLI extends CLI {
    /**
     * Register options and arguments on the given $options object
     *
     * @param Options $options
     * @return void
     */
    protected function setup(Options $options) {
        $options->setHelp('This program lets you find the nth prime products.');
        $options->registerArgument('nth_prime_index', 'The index of the last prime to search for. Required!');
        $options->registerArgument('first_prime_index', 'The index of the first prime to start from. Optional...', False);
        $options->registerOption('perf', 'Run with this option to test performance and display the results.');
    }
    /**
     * Your main program
     *
     * Arguments and options have been parsed when this is run
     *
     * @param Options $options
     * @return void
     */
    protected function main(Options $options) {
        $test_performance = $options->getOpt('perf');
        $args = $options->getArgs();
        $nth_prime_index = $args[0];
        $first_prime_index = isset($args[1]) ? (int) $args[1] - 1 : 0;

        if ($test_performance) {
            Performance::point('Starting the primes algorithm...');
        }
        $atkin_sieve = new AtkinSieve();
        $primes = $atkin_sieve->findPrimes($nth_prime_index, $first_prime_index);
        if ($test_performance) {
            Performance::finish();
        }

        if ($test_performance) {
            Performance::point('Starting the table rendering...');
        }
        try {
            $this->displayTable($primes);
        }
        catch (Exception $ex) {
            $this->error('Table too big! You have to narrow the search...');
        }

        if ($test_performance) {
            Performance::results();
        }
    }

    protected function displayTable($primes) {
        $tf = new TableFormatter();
        $tf->setBorder(' | ');
        $col_width = 5;
        $col_widths = array_fill(0, count($primes) + 1, $col_width);
        $header_row = array_merge(['X'], $primes);

        $this->drawLine($tf);
        echo $tf->format($col_widths, $header_row);
        $this->drawLine($tf);

        foreach ($primes as $prime) {
            $prime_products = array_map(function($v) use ($prime) {
                return $v * $prime;
            }, $primes);
            echo $tf->format($col_widths, array_merge([$prime], $prime_products));
        }
        $this->drawLine($tf);
    }

    protected function drawLine(TableFormatter $tf) {
        echo str_pad('', $tf->getMaxWidth(), '-') . "\n";
    }
}
$cli = new PrimesTableCLI();
$cli->run();