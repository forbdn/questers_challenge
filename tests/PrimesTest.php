<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Questers\AtkinSieve;

class PrimesTest extends TestCase {

    public function testPrimes() {
        $atkin_sieve = new AtkinSieve();
        $this->assertEquals($atkin_sieve->findPrimes(5), AtkinSieve::FIRST_FIVE_PRIMES);
        $this->assertEquals($atkin_sieve->findPrimes(10), array_merge(AtkinSieve::FIRST_FIVE_PRIMES, [13,17,19,23,29]));
        $this->assertEquals($atkin_sieve->findPrimes(20, 10), [31,37,41,43,47,53,59,61,67,71]);
        $this->assertEquals($atkin_sieve->findPrimes(10), $atkin_sieve->findPrimes(10, 0));
    }

}